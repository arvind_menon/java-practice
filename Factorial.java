import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("Finding Factorial of a number: ");
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a Number: ");
        int n = sc.nextInt();
        int a = isFactorial(n);
        System.out.println("Factorial of " +n+ " is: "+a);
        sc.close();
    }

    static int isFactorial(int n)
    {
        int res = 1;
        for(int i=2; i<=n; i++)
        {
            res = res * i; 
        }
        return res;
    }
}
