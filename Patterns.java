public class Patterns {
    public static void main(String[] args) {
        //pattern1();
        /*
            * * * * *
            * * * * *
            * * * * *
            * * * * *
        */
        //pattern2();
        /*
            * * * * *
            *       *
            *       *
            * * * * *
        */
        //pattern3();
        /*
            * 
            * *
            * * *
            * * * * 
        */
        //pattern4();
        /*
            * * * *  
            * * *
            * * 
            *  
        */
        //pattern5();
        /*
                    *
                  * *
                * * *
              * * * *
        */
        //pattern6();
        /*
            1 
            1 2
            1 2 3
            1 2 3 4
            1 2 3 4 5
        */
        //pattern7();
        /*
            1 2 3 4 5
            1 2 3 4
            1 2 3
            1 2 
            1 
        */
        //pattern8();
        /* 
         1 
         2 3 
         4 5 6 
         7 8 9 10 
         11 12 13 14 15 
        */
        pattern9();
        /*
         1
         0 1
         1 0 1
         0 1 0 1
         1 0 1 0 1
         */

    }

    static void pattern1(){
        int n = 4;
        int m = 5;
        for(int i=1; i <= n; i++)
        {
            for (int j=1; j <= m; j++)
            {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

    static void pattern2(){
        int n = 4;
        int m = 5;
        for (int i=1; i<=n; i++)
        {
            for(int j=1; j<=m; j++)
            {
                if(i == 1 || i == n || j == 1 || j == m)
                {
                    System.out.print("* ");
                }
                else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

    static void pattern3(){
        int rows = 4;
        for(int i=1; i<=rows; i++)
        {
            for(int j=1; j<=i; j++)
            {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

    static void pattern4(){
        int rows = 4;
        for(int i=rows; i>=1; i--)
        {
            for(int j=1; j<=i; j++)
            {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

    static void pattern5(){
        int rows = 4;
        for(int i=1; i<=rows; i++)
        {
            for(int j=1; j<=rows-i; j++)
            {
                System.out.print("  ");
            }
            for(int j=1; j<=i; j++)
            {
                System.out.print("* ");
            }
            System.out.println();
        }
    }

    static void pattern6(){
        int rows = 5;
        for(int i=1; i<=rows; i++)
        {
            for(int j=1; j<=i; j++)
            {
                System.out.print(j+ " ");
            }
            System.out.println();
        }
    }

    static void pattern7(){
        int rows = 5;
        for(int i=rows; i>=1; i--)
        {
            for(int j=1; j<=i; j++)
            {
                System.out.print(j+ " ");
            }
            System.out.println();
        }
    }

    static void pattern8(){
        int rows = 5;
        int count = 1;
        for(int i=1; i<=rows; i++)
        { 
            for(int j=1; j<=i; j++)
            {
                System.out.print(count+ " ");
                count++;
            }
            System.out.println();
        }
    }

    static void pattern9(){
        int rows = 5;
        for(int i=1; i<=rows; i++)
        {
            for(int j=1; j<=i; j++)
            {
                if((i+j)%2 == 0)
                {
                    System.out.print("1 ");
                }
                else{
                    System.out.print("0 ");
                }
            }
            System.out.println();
        }
    }

}
