import java.util.Scanner;

public class Arrays {
    public static void main(String[] args) {
        //storeDisplay1D();
        //linearSearch();
        //storeDisplay2D();
        search2D();
    }

    static void storeDisplay1D()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size of the Array: ");
        int num = sc.nextInt();
        
        //Dynamic initialization of the Array
        int[] numbers = new int[num];
        
        for(int i=0; i<numbers.length; i++)
        {
            System.out.print("Enter the "+ i +" index value: ");
            numbers[i] = sc.nextInt();
        }
        System.out.println();
        for(int i=0; i<numbers.length; i++)
        {
            System.out.println("Number at Index "+ i + " is: "+ numbers[i]);
        }

    }

    static void linearSearch()
    {
        Scanner sc = new Scanner(System.in);

        //Static initialization of the Array
        int[] numbers = {25,50,75,100};

        System.out.print("Enter the number to search: ");
        int x = sc.nextInt();

        System.out.println();
        for(int i=0; i<numbers.length; i++)
        {
            if(numbers[i] == x)
            {
                System.out.println("Number found at index: "+ i);
            }
            else
            {
                System.out.println("Number not found!!!");
            }
        }
    }

    static void storeDisplay2D()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of Rows: ");
        int rows = sc.nextInt();
        System.out.print("Enter the number of Columns: ");
        int cols = sc.nextInt(); 

        //2D Array initialization
        int[][] numbers = new int[rows][cols];

        //input
        for(int i=0; i<rows; i++)
        {
            for(int j=0; j<cols; j++)
            {
                numbers[i][j] = sc.nextInt();
            }
        }

        //output
        System.out.println("2D Array: ");
        for(int i=0; i<rows; i++)
        {
            for(int j=0; j<cols; j++)
            {
                System.out.print(numbers[i][j]+ " ");
            }
            System.out.println();
        }

    }

    static void search2D()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of Rows: ");
        int rows = sc.nextInt();
        System.out.print("Enter the number of Columns: ");
        int cols = sc.nextInt(); 

        //2D Array initialization
        int[][] numbers = new int[rows][cols];

        //input
        for(int i=0; i<rows; i++)
        {
            for(int j=0; j<cols; j++)
            {
                numbers[i][j] = sc.nextInt();
            }
        }
        
        System.out.print("Enter the number to search: ");
        int x = sc.nextInt();
        
        //output
        for(int i=0; i<rows; i++)
        {
            for(int j=0; j<cols; j++)
            {
                if( numbers[i][j] == x)
                {
                    System.out.println("Number found at the index: [ "+i + " ][ "+j +" ]");
                }
                else
                {
                    System.out.println("Number not found!!");
                }

            }

        }
    }
}
