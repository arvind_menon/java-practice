import java.util.Scanner;

public class DigitsSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Nth of natural number");
        int n = sc.nextInt();
        int sum = 0;
        for(int i=0; i<=n; i++)
        {
            sum = sum + i;
        }
        System.out.println("Sum till Nth of Natural number:  "+ sum);
    }
}
