public class Sorting{
    public static void main(String[] args) {
        int arr[] = {7,8,3,1,2};
        //bubbleSort(arr);
        //selectionSort(arr);
        insertionSort(arr);
    }

    static void bubbleSort(int arr[])
    {
        //Time Complexity = O(n^2)

        //this loop will go till n-1 because after sorting from 1 to n-1 the last n will be obvisouly lighter and will be placed at the starting
        for(int i=0; i<arr.length-1; i++) 
        {
            //this loop will go till n-i-1 because after 1st loop -> number placed at last. after 2nd loop -> no need to loop till last rather place the number at 2nd last
            for(int j=0; j<arr.length-i-1; j++)
            {
                if(arr[j] > arr[j+1])  //for descending order change this condition as if(arr[j] < arr[j+1])
                {
                    //swap
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }

        for(int i=0; i<arr.length; i++)
        {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();
    }

    static void selectionSort(int arr[])
    {
        //Time Complexity = O(n^2)
        for(int i=0; i<arr.length-1; i++)
        {
            int smallest = i;
            for(int j=i+1; j<arr.length; j++)
            {
                if(arr[smallest] > arr[j])
                {
                    smallest = j;
                }
            }
            int temp = arr[smallest];
            arr[smallest] = arr[i];
            arr[i] = temp; 
        }
        for(int i=0; i<arr.length; i++)
        {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();

    }

    static void insertionSort(int arr[])
    {
        //Time Complexity = O(n^2)
        for(int i=1; i<arr.length; i++)  //Unsorted part is from i=1 to array length
        {
            int current = arr[i];  // Since arr[0] we are assuming as sorted already so we will take arr[i] as current element that needs to be sorted
            int j = i-1 ;  // To track the sorted part we are using j.
            while(j >= 0 && current < arr[j])
            {
                arr[j+1] = arr[j];
                j--;

            }
            //placement of the element
            arr[j+1] = current;
        }
        for(int i=0; i<arr.length; i++)
        {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();
    }
}