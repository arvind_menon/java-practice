public class Palindrome {
    public static void main(String[] args) {
        System.out.println("Finding Palindrome!");
        isPalindrome(123321);

    }

    static void isPalindrome(int n)
    {
        int temp = n;
        int res = 0;
        while(temp != 0)
        {
            //modulo by 10
            int a = temp % 10; 

            //add to new num
            res = res * 10 + a;

            //divide by 10
            temp = temp / 10;
        }
        if(res == n)
            System.out.println(n + " is a Palindrome");
        else
            System.out.println(n + " is Not a Palindrome");

    }
}
