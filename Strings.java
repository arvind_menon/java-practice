public class Strings {
    public static void main(String[] args) {
        String firstName = "tony";
        String lastName = "stark";
        String fullName = firstName + "@" + lastName;
        // tony@stark
        System.out.println("Length of full name: "+ fullName.length());
        letterAtIndex(fullName);

        String name1 = "aahello";
        String name2 = "aabello";
        /* 
            compareTo function will check the two strings and return as such:
            s1 > s2 : +ve value
            s1 == s2 : 0
            s1 < s2 : -ve value

            Here aa will be same and now h vs b will happen and since h comes later in the alphabetic order,
            aahello will be considered as the large String. 
        */
        stringComparison(name1, name2);
        
    }

    static void letterAtIndex(String name)
    {
        for(int i=0; i<name.length(); i++)
        {
            System.out.println("Letter at index " + i +": "+ name.charAt(i));
        }
    }

    static void stringComparison(String m, String n)
    {
        if(m.compareTo(n) == 0)
            System.out.println("Strings are equal !! ");
        else 
            System.out.println("String are not equal !! ");

        if(new String("Tony") == new String("Tony")){
            System.out.println("Strings are equal !! ");
        }
        else{
            System.out.println("String are not equal !! ");
        }
    }
}
