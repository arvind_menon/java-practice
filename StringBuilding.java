public class StringBuilding {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("Tony");
        System.out.println(sb);
        changeCharacter(sb);
        appendingCharacter();
        reverseString();
        reverseLoopString();
    }

    static void changeCharacter(StringBuilder name)
    {
        System.out.println("Char at index 0: " + name.charAt(0));
        
        name.setCharAt(0, 'P'); // this changes the char at 0 with the letter P
        System.out.println(name);
        
        name.insert(0,'G'); // this will insert G at index 0 and entire PONY will be shifted to next indices
        System.out.println(name);

        name.delete(1, 2); // this will start from index 1 and before index 2 it will take the char and will delete it
        System.out.println(name);
    }

    static void appendingCharacter()
    {
        StringBuilder sb = new StringBuilder("h");
        sb.append("e"); // str + "e"; -> if we are using Strings
        sb.append("l"); // str + "l"; -> Strings will delete the he string and will now point to new string hel from stack to heap
        sb.append("l"); // StringBuilder won't delete the string rather it will modify it and same string will point from stack to heap
        sb.append("o");
        System.out.println(sb);
        System.out.println("Length of the string: "+sb.length());
    }

    static void reverseString()
    {
        StringBuilder name = new StringBuilder("Captain America");
        name.reverse();
        System.out.println(name);
    }

    static void reverseLoopString()
    {
        StringBuilder name1 = new StringBuilder("hello");
        for(int i=0; i<name1.length()/2; i++)  // logic is we will take first half of hello and back half and will interchange them
        {
            int front = i;
            int back = name1.length() - 1 - i;  // 5 - 1 - 0 = 4 which is o in hello

            char frontChar = name1.charAt(front);
            char backChar = name1.charAt(back);

            name1.setCharAt(front, backChar);
            name1.setCharAt(back, frontChar);
        }
        System.out.println(name1);
    }
}
