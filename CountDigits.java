import static java.lang.Math.*;
public class CountDigits {
    public static void main(String[] args) {
        int num = numDigits(12345);
        System.out.println(num);
    }

    static int numDigits(long n)
    {
        if(n==0)
            return 0;
        return 1+numDigits(n/10);
    }

}